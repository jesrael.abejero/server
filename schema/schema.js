const graphql = require('graphql');

const Devices = require('../models/device')
const DeviceTypes = require('../models/deviceType')

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLList,
    GraphQLID,
    GraphQLNonNull
} = graphql;

const Device = new GraphQLObjectType({
    name: 'Devices',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        ip: { type: GraphQLString },
        date: { type: GraphQLString },
        deviceId: {
            type: DeviceType,
            resolve(parent, args) {
                return DeviceTypes.findById(parent.deviceId)
            }
        }
    })
});

const DeviceType = new GraphQLObjectType({
    name: 'DeviceTypes',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        devices: {
            type: new GraphQLList(Device),
            resolve(parent, args) {
                return Devices.find({ deviceId: parent.id })
            }
        }
    })
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        device: {
            type: Device,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                return Devices.findById(args.id);
            }
        },
        deviceType: {
            type: DeviceType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                return DeviceTypes.findById(args.id);
            }
        },
        devices: {
            type: new GraphQLList(Device),
            resolve(parent, args) {
                return Devices.find({});
            }
        },
        deviceTypes: {
            type: new GraphQLList(DeviceType),
            resolve(parent, args) {
                return DeviceTypes.find({});
            }
        }
    }
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addDeviceType: {
            type: DeviceType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                let deviceType = new DeviceTypes({
                    name: args.name
                });
                return deviceType.save();
            }
        },
        addDevice: {
            type: Device,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                ip: { type: new GraphQLNonNull(GraphQLString) },
                deviceId: { type: new GraphQLNonNull(GraphQLID) },
                date: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args) {
                let device = new Devices({
                    name: args.name,
                    ip: args.ip,
                    deviceId: args.deviceId,
                    date: args.date
                });
                return device.save();
            }
        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})