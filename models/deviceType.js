const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const deviceTypeSchema = new Schema({
    name: String
});

module.exports = mongoose.model('DeviceType', deviceTypeSchema)