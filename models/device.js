const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const deviceSchema = new Schema({
    name: String,
    ip: String,
    deviceId: String,
    date: String
});

module.exports = mongoose.model('Device', deviceSchema)